# DEEP Repository
> Repository for the Distribution of Energetic Electron and Proton (DEEP) project.

### Gate Simulation
Consist of simulation setup 1 to 4, proton/electron range simulation setup, <br />
simulation setup wide, simulation setup baffle, simulation setup mask,  <br />
2 mm thick silicon layer simulation setup, and physics list validation simulation setup. <br />
More detailed information: [gate_simulation](https://git.app.uib.no/Hogne.Andersen/DEEP/tree/master/gate_simulation/)

### Testbench Framework
Testbench framework for the DEEP DSP device. <br />
More detailed information: [testbench_framework](https://git.app.uib.no/Hogne.Andersen/DEEP/tree/master/testbench_framework/)

### Repository structure

    ├── gate_simulation                     # GATE simulation setups
    │
    └── testbench_framework                 # VHDL testbench framework for the DEEP DSP