# vsim deep_dsp.deep_dsp_tb 
# Start time: 15:44:10 on Jun 07,2018
# Loading std.standard
# Loading std.textio(body)
# Loading ieee.std_logic_1164(body)
# Loading ieee.numeric_std(body)
# Loading std.env(body)
# Loading uvvm_util.types_pkg(body)
# Loading uvvm_util.adaptations_pkg(body)
# Loading uvvm_util.string_methods_pkg(body)
# Loading uvvm_util.hierarchy_linked_list_pkg(body)
# Loading ieee.math_real(body)
# Loading uvvm_util.license_pkg(body)
# Loading uvvm_util.protected_types_pkg(body)
# Loading uvvm_util.alert_hierarchy_pkg(body)
# Loading uvvm_util.methods_pkg(body)
# ** Note: 
# 
# 
# *****************************************************************************************************
#  This is a *** LICENSED PRODUCT *** as given in the LICENSE.TXT in the root directory.
# *****************************************************************************************************
# 
# 
#    Time: 0 ns  Iteration: 0  Region: /methods_pkg File: ../../deep_dsp/../uvvm_util/../uvvm_util/src/methods_pkg.vhd
# ** Note: 
# 
# =====================================================================================================
# =====================================================================================================
# This info section may be turned off via C_SHOW_UVVM_UTILITY_LIBRARY_INFO in adaptations_pkg.vhd
# 
# Important Simulator setup: 
# - Set simulator to break on severity 'FAILURE' 
# - Set simulator transcript to a monospace font (e.g. Courier new)
# 
# UVVM Utility Library setup:
# - It is recommended to go through the two powerpoint presentations provided with the download
# - There is a Quick-Reference in the doc-directory
# - In order to change layout or behaviour - please check the src*/adaptations_pkg.vhd
#   This is intended for personal or company customization
# 
# License conditions are given in LICENSE.TXT
# =====================================================================================================
# =====================================================================================================
# 
# 
#    Time: 0 ns  Iteration: 0  Region: /methods_pkg File: ../../deep_dsp/../uvvm_util/../uvvm_util/src/methods_pkg.vhd
# Loading uvvm_util.bfm_common_pkg(body)
# Loading bitvis_vip_sbi.sbi_bfm_pkg(body)
# Loading deep_dsp.deep_dsp_tb(tb)
# Loading deep_dsp.deep_dsp(rtl_deep_dsp)
# UVVM:      
# UVVM:      --------------------------------------------------------------------------------------------------------------------------------------------------------------------
# UVVM:      ***  REPORT OF GLOBAL CTRL ***
# UVVM:      --------------------------------------------------------------------------------------------------------------------------------------------------------------------
# UVVM:                                IGNORE    STOP_LIMIT                      
# UVVM:                NOTE         :  REGARD         0    
# UVVM:                TB_NOTE      :  REGARD         0    
# UVVM:                WARNING      :  REGARD         0    
# UVVM:                TB_WARNING   :  REGARD         0    
# UVVM:                MANUAL_CHECK :  REGARD         0    
# UVVM:                ERROR        :  REGARD         1    
# UVVM:                TB_ERROR     :  REGARD         1    
# UVVM:                FAILURE      :  REGARD         1    
# UVVM:                TB_FAILURE   :  REGARD         1    
# UVVM:      --------------------------------------------------------------------------------------------------------------------------------------------------------------------
# UVVM:      
# UVVM:      
# UVVM:      --------------------------------------------------------------------------------------------------------------------------------------------------------------------
# UVVM:      ***  REPORT OF MSG ID PANEL ***
# UVVM:      --------------------------------------------------------------------------------------------------------------------------------------------------------------------
# UVVM:                ID                             Status
# UVVM:                ------------------------       ------
# UVVM:                ID_UTIL_BURIED               : DISABLED    
# UVVM:                ID_BITVIS_DEBUG              : DISABLED    
# UVVM:                ID_UTIL_SETUP                : ENABLED    
# UVVM:                ID_LOG_MSG_CTRL              : ENABLED    
# UVVM:                ID_ALERT_CTRL                : ENABLED    
# UVVM:                ID_FINISH_OR_STOP            : ENABLED    
# UVVM:                ID_CLOCK_GEN                 : ENABLED    
# UVVM:                ID_GEN_PULSE                 : ENABLED    
# UVVM:                ID_BLOCKING                  : ENABLED    
# UVVM:                ID_POS_ACK                   : ENABLED    
# UVVM:                ID_LOG_HDR                   : ENABLED    
# UVVM:                ID_LOG_HDR_LARGE             : ENABLED    
# UVVM:                ID_LOG_HDR_XL                : ENABLED    
# UVVM:                ID_SEQUENCER                 : ENABLED    
# UVVM:                ID_SEQUENCER_SUB             : ENABLED    
# UVVM:                ID_BFM                       : ENABLED    
# UVVM:                ID_BFM_WAIT                  : ENABLED    
# UVVM:                ID_BFM_POLL                  : ENABLED    
# UVVM:                ID_BFM_POLL_SUMMARY          : ENABLED    
# UVVM:                ID_TERMINATE_CMD             : ENABLED    
# UVVM:                ID_SEGMENT_INITIATE          : ENABLED    
# UVVM:                ID_SEGMENT_COMPLETE          : ENABLED    
# UVVM:                ID_SEGMENT_HDR               : ENABLED    
# UVVM:                ID_SEGMENT_DATA              : ENABLED    
# UVVM:                ID_PACKET_INITIATE           : ENABLED    
# UVVM:                ID_PACKET_COMPLETE           : ENABLED    
# UVVM:                ID_PACKET_HDR                : ENABLED    
# UVVM:                ID_PACKET_DATA               : ENABLED    
# UVVM:                ID_FRAME_INITIATE            : ENABLED    
# UVVM:                ID_FRAME_COMPLETE            : ENABLED    
# UVVM:                ID_FRAME_HDR                 : ENABLED    
# UVVM:                ID_FRAME_DATA                : ENABLED    
# UVVM:                ID_COVERAGE_MAKEBIN          : DISABLED    
# UVVM:                ID_COVERAGE_ADDBIN           : DISABLED    
# UVVM:                ID_COVERAGE_ICOVER           : DISABLED    
# UVVM:                ID_COVERAGE_CONFIG           : ENABLED    
# UVVM:                ID_COVERAGE_SUMMARY          : ENABLED    
# UVVM:                ID_COVERAGE_HOLES            : ENABLED    
# UVVM:                ID_UVVM_SEND_CMD             : ENABLED    
# UVVM:                ID_UVVM_CMD_ACK              : ENABLED    
# UVVM:                ID_UVVM_CMD_RESULT           : ENABLED    
# UVVM:                ID_CMD_INTERPRETER           : ENABLED    
# UVVM:                ID_CMD_INTERPRETER_WAIT      : ENABLED    
# UVVM:                ID_IMMEDIATE_CMD             : ENABLED    
# UVVM:                ID_IMMEDIATE_CMD_WAIT        : ENABLED    
# UVVM:                ID_CMD_EXECUTOR              : ENABLED    
# UVVM:                ID_CMD_EXECUTOR_WAIT         : ENABLED    
# UVVM:                ID_INSERTED_DELAY            : ENABLED    
# UVVM:                ID_UVVM_DATA_QUEUE           : ENABLED    
# UVVM:                ID_CONSTRUCTOR               : ENABLED    
# UVVM:                ID_CONSTRUCTOR_SUB           : ENABLED    
# UVVM:      --------------------------------------------------------------------------------------------------------------------------------------------------------------------
# UVVM:      
# UVVM: ID_LOG_MSG_CTRL                    0.0 ns  TB seq.              enable_log_msg(ALL_MESSAGES). 
# UVVM: 
# UVVM: 
# UVVM: ID_LOG_HDR                         0.0 ns  TB seq.              Start Simulation of TB for DEEP DSP Device
# UVVM: -------------------------------------------------------------------------------------------------------------------------------------------------------------------------
# UVVM: ID_SEQUENCER_SUB                   0.0 ns  TB seq.              All inputs set passive.
# UVVM: ID_SEQUENCER_SUB                  60.0 ns  TB seq.              Pulsed reset-signal - active for 5T
# UVVM: 
# UVVM: 
# UVVM: ID_LOG_HDR                        60.0 ns  TB seq.              Check default values:
# UVVM: -------------------------------------------------------------------------------------------------------------------------------------------------------------------------
# UVVM: ID_POS_ACK                        60.0 ns  TB seq.              check_value() => OK, for unsigned x"00000000"' (exp: x"00"'). 'deep_dsp_out should be zero'
# UVVM: ID_SEQUENCER_SUB                  60.0 ns  TB seq.              Default values checked
# UVVM: 
# UVVM: 
# UVVM: ID_LOG_HDR                        60.0 ns  TB seq.              Enable device and run for a while:
# UVVM: -------------------------------------------------------------------------------------------------------------------------------------------------------------------------
# UVVM: ID_SEQUENCER_SUB                  95.0 ns  TB seq.              Enabled device
# UVVM: ID_SEQUENCER_SUB                 595.0 ns  TB seq.              Disabled device
# UVVM: 
# UVVM: 
# UVVM: ID_LOG_HDR                      1095.0 ns  TB seq.              Clear device and check values:
# UVVM: -------------------------------------------------------------------------------------------------------------------------------------------------------------------------
# UVVM: ID_POS_ACK                      1195.0 ns  TB seq.              check_value() => OK, for unsigned x"00000000"' (exp: x"00"'). 'deep_dsp_out should be zero'
# UVVM: 
# UVVM: 
# UVVM: ID_LOG_HDR                      1195.0 ns  TB seq.              Set a value to the input and check output values:
# UVVM: -------------------------------------------------------------------------------------------------------------------------------------------------------------------------
# UVVM: ID_SEQUENCER_SUB                1195.0 ns  TB seq.              Enabled device
# UVVM: ID_SEQUENCER_SUB                1245.0 ns  TB seq.              Disabled device
# UVVM: ID_POS_ACK                      1295.0 ns  TB seq.              check_value() => OK, for unsigned x"FFFFFFFF"'. 'deep_dsp_out should be zero'
# UVVM: 
# UVVM: 
# UVVM: ID_LOG_HDR                      1295.0 ns  TB seq.              Clear device and check values:
# UVVM: -------------------------------------------------------------------------------------------------------------------------------------------------------------------------
# UVVM: ID_POS_ACK                      1395.0 ns  TB seq.              check_value() => OK, for unsigned x"00000000"' (exp: x"00"'). 'deep_dsp_out should be zero'
# UVVM:      
# UVVM:      ====================================================================================================================================================================
# UVVM:      *** FINAL SUMMARY OF ALL ALERTS  ***      
# UVVM:      ====================================================================================================================================================================
# UVVM:                                REGARDED   EXPECTED  IGNORED      Comment?
# UVVM:                NOTE         :      0         0         0         ok      
# UVVM:                TB_NOTE      :      0         0         0         ok      
# UVVM:                WARNING      :      0         0         0         ok      
# UVVM:                TB_WARNING   :      0         0         0         ok      
# UVVM:                MANUAL_CHECK :      0         0         0         ok      
# UVVM:                ERROR        :      0         0         0         ok      
# UVVM:                TB_ERROR     :      0         0         0         ok      
# UVVM:                FAILURE      :      0         0         0         ok      
# UVVM:                TB_FAILURE   :      0         0         0         ok      
# UVVM:      ====================================================================================================================================================================
# UVVM:      >> Simulation SUCCESS: No mismatch between counted and expected serious alerts
# UVVM:      ====================================================================================================================================================================
# UVVM:      
# UVVM:      
# UVVM: 
# UVVM: 
# UVVM: ID_LOG_HDR                      1495.0 ns  TB seq.              SIMULATION COMPLETED
# UVVM: -------------------------------------------------------------------------------------------------------------------------------------------------------------------------