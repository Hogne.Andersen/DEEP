--========================================================================================================================
--  File name: deep_dsp.vhdl
--  Author: Hogne Andersen
--  Description: DEEP DSP Device
--  VHDL standard used: 2008
--========================================================================================================================


library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;


--! DEEP DSP Device
entity deep_dsp is
  generic (
    constant dWidth  :  positive  := 32  --! Data width
  );	

  port (									
    clk              :  in std_logic;                                           --! Clock
    areset_n         :  in std_logic;                                           --! Asynchronous reset    
    deep_dsp_enable  :  in std_logic;                                           --! Start / stop
    deep_dsp_clear   :  in std_logic;                                           --! Synchronous clear
    deep_dsp_in      :  in unsigned((dWidth-1) downto 0)   := (others => '0');  --! Parallel input    
    deep_dsp_out     :  out unsigned((dWidth-1) downto 0)  := (others => '0')   --! Parallel output
  );
end deep_dsp;


architecture RTL_deep_dsp of deep_dsp is
  signal tmp_out : unsigned((dWidth-1) downto 0) := (others => '0');  --! Temporary value
  
begin

  p_device : process(clk, areset_n)
  begin
    -- Asynchronous reset
    if (areset_n = '0') then
      tmp_out <= (others => '0');
    elsif rising_edge(clk) then
      -- Synchronous reset
      if (deep_dsp_clear = '1') then
        tmp_out <= (others => '0');
      -- Output equals input
      elsif (deep_dsp_enable = '1') then
        tmp_out <= (deep_dsp_in);
      end if;
    end if;
  end process p_device;
  
  deep_dsp_out <= tmp_out;
  
end RTL_deep_dsp;
