--========================================================================================================================
--  File name: deep_dsp_tb.vhdl
--  Author: Hogne Andersen
--  Description: Bitvis Test Bench (tb) to the DEEP DSP Device
--  VHDL standard used: 2008
--========================================================================================================================

--========================================================================================================================
-- Copyright (c) 2017 by Bitvis AS.  All rights reserved.
-- You should have received a copy of the license file containing the MIT License (see LICENSE.TXT), if not, 
-- contact Bitvis AS <support@bitvis.no>.
--
-- UVVM AND ANY PART THEREOF ARE PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUdatainG BUT NOT LIMITED TO THE
-- WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
-- OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
-- OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH UVVM OR THE USE OR OTHER DEALINGS IN UVVM.
--========================================================================================================================


library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

library STD;
use std.env.all;

library uvvm_util;
context uvvm_util.uvvm_util_context;

library bitvis_vip_sbi;
use bitvis_vip_sbi.sbi_bfm_pkg.all;

-- Test case entity
entity deep_dsp_tb is
  generic (
    constant  dWidth   :  positive  := 32  --! Data width
  );	
end entity deep_dsp_tb;


-- Test case architecture
architecture tb of deep_dsp_tb is
  -- DSP interface and general control signals
  signal clk       :  std_logic  := '0';  -- Clock  
  signal areset_n  :  std_logic  := '0';  -- Asynchronous reset      
  
  -- DEEP DSP DUT  
  signal deep_dsp_enable  :  std_logic;                                           --! Start / stop
  signal deep_dsp_clear   :  std_logic;                                           --! Synchronous clear
  signal deep_dsp_in      :  unsigned((dWidth-1) downto 0)   := (others => '0');  --! Parallel input    
  signal deep_dsp_out     :  unsigned((dWidth-1) downto 0)  := (others => '0');   --! Parallel output
  
  -- CPU interface
  signal sbi_if : t_sbi_if(addr(3 downto 0), wdata(dWidth-1 downto 0), rdata(dWidth-1 downto 0)) := init_sbi_if_signals(4, dWidth);
  signal clock_ena  : boolean := false;
  constant C_CLK_PERIOD : time := 10 ns;


  procedure clock_gen(
    signal   clock_signal  :  inout std_logic;
    signal   clock_ena     :  in    boolean;
    constant clock_period  :  in    time
  ) is
    variable v_first_half_clk_period : time := C_CLK_PERIOD / 2;
  begin
    loop
      if not clock_ena then
        wait until clock_ena;
      end if;
      wait for v_first_half_clk_period;
      clock_signal <= not clock_signal;
      wait for (clock_period - v_first_half_clk_period);
      clock_signal <= not clock_signal;
    end loop;
  end;
 
  
begin

  -----------------------------------------------------------------------------
  -- Instantiate DUT
  ----------------------------------------------------------------------------- 
  m_dut : entity work.deep_dsp 
    generic map(
      dWidth   =>  dWidth
    )
    
  port map(
    clk       =>  clk,
    areset_n  =>  areset_n,
    
    -- DEEP DSP Device
    deep_dsp_enable => deep_dsp_enable,
    deep_dsp_clear  => deep_dsp_clear,
    deep_dsp_in     => deep_dsp_in,  
    deep_dsp_out    => deep_dsp_out
  );
  
    sbi_if.ready <= '1'; -- always ready in the same clock cycle.
  
  -- Set up clock generator
  clock_gen(clk, clock_ena, 10 ns);  
  
  p_main: process
    constant C_SCOPE : string  := C_TB_SCOPE_DEFAULT;

    procedure pulse(
      signal   target        :  inout std_logic;
      signal   clock_signal  :  in    std_logic;
      constant num_periods   :  in    natural;
      constant msg           :  in    string
    ) is
    begin
      if num_periods > 0 then
        wait until falling_edge(clock_signal);
        target  <= '1';
        for i in 1 to num_periods loop
          wait until falling_edge(clock_signal);
        end loop;
      else
        target  <= '1';
        wait for 0 ns;  -- Delta cycle only
      end if;
      target  <= '0';
      log(ID_SEQUENCER_SUB, msg, C_SCOPE);
    end;

    procedure pulse(
      signal   target        :  inout  std_logic_vector;
      constant pulse_value   :  in     std_logic_vector;
      signal   clock_signal  :  in     std_logic;
      constant num_periods   :  in     natural;
      constant msg           :  in     string
      ) is
    begin
      if num_periods > 0 then
        wait until falling_edge(clock_signal);
        target <= pulse_value;
        for i in 1 to num_periods loop
          wait until falling_edge(clock_signal);
        end loop;
      else
        target <= pulse_value;
        wait for 0 ns;  -- Delta cycle only
      end if;
      target(target'range) <= (others => '0');
      log(ID_SEQUENCER_SUB, "Pulsed to " & to_string(pulse_value, HEX, AS_IS, INCL_RADIX) & ". " & add_msg_delimiter(msg), C_SCOPE);
    end;

    
    -- Log overloads for simplification
    procedure log(
      msg : string
      ) is
    begin
      log(ID_SEQUENCER, msg, C_SCOPE);
    end;

    
    -- Procedure set all inputs passive
    procedure set_inputs_passive(
      dummy   : t_void
      ) is
    begin
      deep_dsp_clear   <= '0';  
      deep_dsp_enable  <= '0'; 
      log(ID_SEQUENCER_SUB, "All inputs set passive.", C_SCOPE);
    end;

    variable v_time_stamp   : time := 0 ns;


  begin

    -- Print the configuration to the log
    report_global_ctrl(VOID);
    report_msg_id_panel(VOID);

    enable_log_msg(ALL_MESSAGES);
    --disable_log_msg(ALL_MESSAGES);
    --enable_log_msg(ID_LOG_HDR);

    
    log(ID_LOG_HDR, "Start Simulation of TB for DEEP DSP Device", C_SCOPE);
    ------------------------------------------------------------
    set_inputs_passive(VOID);
    clock_ena <= true;   -- to start clock generator
    pulse(areset_n, clk, 5, "Pulsed reset-signal - active for 5T"); -- resets DUT
    v_time_stamp := now;

    
    log(ID_LOG_HDR, "Check default values:", C_SCOPE);
    ------------------------------------------------------------ 
    check_value(deep_dsp_out, x"00", ERROR, "deep_dsp_out should be zero", C_SCOPE);
    log(ID_SEQUENCER_SUB, "Default values checked", C_SCOPE);    


    log(ID_LOG_HDR, "Enable device and run for a while:", C_SCOPE);
    ------------------------------------------------------------
    -- Wait 4 rising edges
    wait_num_rising_edge(clk, 4);
    -- Do not reset
    areset_n <= '1';
    deep_dsp_enable <= '1';
    log(ID_SEQUENCER_SUB, "Enabled device", C_SCOPE);
    wait_num_rising_edge(clk, 50);
    deep_dsp_enable <= '0';
    log(ID_SEQUENCER_SUB, "Disabled device", C_SCOPE);    
    wait_num_rising_edge(clk, 50);
 
    log(ID_LOG_HDR, "Clear device and check values:", C_SCOPE);
    ------------------------------------------------------------    
    deep_dsp_clear <= '1';   
    wait_num_rising_edge(clk, 5);
    deep_dsp_clear <= '0';    
    wait_num_rising_edge(clk, 5);    
    check_value(deep_dsp_out, x"00", ERROR, "deep_dsp_out should be zero", C_SCOPE);       

    log(ID_LOG_HDR, "Set a value to the input and check output values:", C_SCOPE);
    ------------------------------------------------------------    
    deep_dsp_in <= (others => '1');   
    deep_dsp_enable <= '1';
    log(ID_SEQUENCER_SUB, "Enabled device", C_SCOPE);
    wait_num_rising_edge(clk, 5);
    deep_dsp_enable <= '0';
    log(ID_SEQUENCER_SUB, "Disabled device", C_SCOPE);
    wait_num_rising_edge(clk, 5);  
    check_value(deep_dsp_out, x"FFFFFFFF", ERROR, "deep_dsp_out should be zero", C_SCOPE); 

    log(ID_LOG_HDR, "Clear device and check values:", C_SCOPE);
    ------------------------------------------------------------    
    deep_dsp_clear <= '1';   
    wait_num_rising_edge(clk, 5);
    deep_dsp_clear <= '0';    
    wait_num_rising_edge(clk, 5);    
    check_value(deep_dsp_out, x"00", ERROR, "deep_dsp_out should be zero", C_SCOPE);     
    
    --==================================================================================================
    -- Ending the simulation
    --------------------------------------------------------------------------------------
    
    wait for 100 ns;              -- to allow some time for completion
    report_alert_counters(FINAL); -- Report final counters and print conclusion for simulation (Success/Fail)
    log(ID_LOG_HDR, "SIMULATION COMPLETED", C_SCOPE);
    
    -- Finish the simulation
    std.env.stop;
    wait;  -- to stop completely

  end process p_main;  
  
end architecture tb;