onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider Clock,aReset
add wave -noupdate  sim:/deep_dsp_tb/clk
add wave -noupdate  sim:/deep_dsp_tb/areset_n

add wave -noupdate -divider DataWidth
add wave -noupdate  sim:/deep_dsp_tb/dWidth

add wave -noupdate -divider Enable,Clear
add wave -noupdate  sim:/deep_dsp_tb/deep_dsp_enable
add wave -noupdate  sim:/deep_dsp_tb/deep_dsp_clear

add wave -noupdate -divider Input,Output
add wave -noupdate  sim:/deep_dsp_tb/deep_dsp_in
add wave -noupdate  sim:/deep_dsp_tb/deep_dsp_out

TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {0 ns} 0}
quietly wave cursor active 0
configure wave -namecolwidth 190
configure wave -valuecolwidth 220
configure wave -justifyvalue left
configure wave -signalnamewidth 2
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ns} {6100 ns}