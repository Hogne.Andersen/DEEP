# Testbench Framework Repository
> For the Distribution of Energetic Electron and Proton Instrument

### Required software
ModelSim - Intel FPGA Starter Edition - Version 10.5b <br />
Note: Similar software can also be used.

#### To start a new test:
Start ModelSim and change directory to .../deep_dsp/script folder and write
   ```
   do compile_and_sim_all.do
   ```
in the transcript window. Compilation and the simulation will now start.

### Repository structure

    ├── deep_dsp                            # testbench framework
    │   ├── doc                             # simulation report
    │   ├── script                          # scripts for automation 
    │   ├── sim                             # temporary simulation files
    │   ├── src                             # .vhdl file for the device
    │   ├── tb                              # testbench file for the device
    │   └── README.md
    │
    ├── bitvis_vip_sbi                      # an example vip that uses uvvm (testbench library)
    |
    └── uvvm_util                           # bitvis uvvm utility library (testbench library)
    