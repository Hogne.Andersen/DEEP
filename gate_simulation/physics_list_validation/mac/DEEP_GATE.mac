#####################################################################
##  File name: DEEP_GATE.mac                                         
##  Author: Hogne Andersen                                      
##  Description: Physics List Validation setup.     
##  GATE version: 8.1                                          
#####################################################################


#####################################################################
#   V I S U A L I S A T I O N                                   
#####################################################################
# Define visualisation elements
/vis/open                                   OGLSQt
/vis/viewer/zoom                            1
/vis/viewer/set/viewpointThetaPhi           120 120
/vis/viewer/set/style                       surface
/tracking/storeTrajectory                   1
/vis/scene/endOfEventAction accumulate      25000
/vis/drawVolume
/vis/viewer/update
/vis/viewer/refresh 


#####################################################################
#   M A T E R I A L S                                           
#####################################################################
# Define where the material database is
/gate/geometry/setMaterialDatabase data/GateMaterials.db


##################################################################### 
#   W O R L D                                                   
#####################################################################
# Define the world dimensions
/gate/world/geometry/setXLength     45. mm
/gate/world/geometry/setYLength     45. mm
/gate/world/geometry/setZLength     45. mm
/gate/world/setMaterial             Vacuum
/gate/world/vis/setColor            gray
/gate/world/vis/forceWireframe      1
/gate/world/vis/setVisible          1


#####################################################################
#   D E E P    I N S T R U M E N T                              
#####################################################################
# Create the DEEP Instrument
/gate/world/daughters/name                          scanner
/gate/world/daughters/insert                        box
# Define the dimensions, position and material
/gate/scanner/geometry/setXLength                   15. mm
/gate/scanner/geometry/setYLength                   15. mm
/gate/scanner/geometry/setZLength                   2.4 mm
/gate/scanner/placement/setTranslation              0 0 0 mm
/gate/scanner/vis/setColor                          red
/gate/scanner/vis/forceWireframe                    1
/gate/scanner/vis/setVisible                        0

# Define the dimensions, position and material
/gate/scanner/daughters/name                        SiFront
/gate/scanner/daughters/insert                      box
/gate/SiFront/geometry/setXLength                   15. mm
/gate/SiFront/geometry/setYLength                   15. mm
/gate/SiFront/geometry/setZLength                   1. mm
/gate/SiFront/placement/setTranslation              0 0 -0.7 mm
/gate/SiFront/setMaterial                           Silicon
/gate/SiFront/vis/setColor                          yellow
/gate/SiFront/vis/forceWireframe                    1
/gate/SiFront/vis/setVisible                        1

# Define the dimensions, position and material
/gate/scanner/daughters/name                        SiBack
/gate/scanner/daughters/insert                      box
/gate/SiBack/geometry/setXLength                    15. mm
/gate/SiBack/geometry/setYLength                    15. mm
/gate/SiBack/geometry/setZLength                    1. mm
/gate/SiBack/placement/setTranslation               0 0 0.7 mm
/gate/SiBack/setMaterial                            Silicon
/gate/SiBack/vis/setColor                           yellow
/gate/SiBack/vis/forceWireframe                     1
/gate/SiBack/vis/setVisible                         1

# Attach all layers to scanner
/gate/SiFront/attachCrystalSD
/gate/SiBack/attachCrystalSD


#####################################################################
#   P H A N T O M    S O U R C E                                
#####################################################################
# Create a phantom for the source for easier placement 
/gate/world/daughters/name                          phantomSource
/gate/world/daughters/insert                        box
# Define the dimensions, position and material  
/gate/phantomSource/geometry/setXLength             5 mm
/gate/phantomSource/geometry/setYLength             5 mm
/gate/phantomSource/geometry/setZLength             1 mm
/gate/phantomSource/placement/setTranslation        2. 0. -19.5 mm 
/gate/phantomSource/placement/setRotationAxis       1 0 0 
/gate/phantomSource/placement/setRotationAngle      180 deg  
/gate/phantomSource/setMaterial                     Vacuum
/gate/phantomSource/vis/setColor                    green
/gate/phantomSource/vis/forceWireframe              1
/gate/phantomSource/vis/setVisible                  1


#####################################################################
#   P H Y S I C S                                               
#####################################################################
# Define physics to be emstandard_opt4 QGSP_BIC__SS
/gate/physics/addPhysicsList emstandard_opt4


#####################################################################
#   I N I T I A L I Z E                                         
#####################################################################
/gate/run/initialize


#####################################################################
#   S O U R C E                                                     
#####################################################################
# Define a uniform mono energy electron beam
/gate/source/addSource                              uniformBeam gps
/gate/source/uniformBeam/gps/particle               e-
/gate/source/uniformBeam/gps/energytype             Mono
/gate/source/uniformBeam/gps/monoenergy             {energy} keV
/gate/source/uniformBeam/gps/type                   Beam
/gate/source/uniformBeam/gps/shape                  Square
/gate/source/uniformBeam/gps/pos/centre             0 0 0 mm
/gate/source/uniformBeam/gps/direction              0 0 1
/gate/source/uniformBeam/gps/halfx                  .5 mm
/gate/source/uniformBeam/gps/halfy                  2.5 mm
/gate/source/uniformBeam/gps/centre                 0. 0. 0. mm
/gate/source/uniformBeam/gps/angtype                iso
/gate/source/uniformBeam/gps/mintheta               0 deg
/gate/source/uniformBeam/gps/maxtheta               0 deg
/gate/source/uniformBeam/gps/minphi                 0 deg
/gate/source/uniformBeam/gps/maxphi                 360 deg
/gate/source/uniformBeam/attachTo                   phantomSource
/gate/application/setTotalNumberOfPrimaries         {particles}
/gate/source/uniformBeam/visualize                  {particles} green 1 


#####################################################################
#   O U T P U T                                                 
#####################################################################
# Define output as ROOT file
/gate/output/root/enable # enable root output file
/gate/output/root/setFileName output/DEEP_OUTPUT

/gate/output/root/setRootHitFlag 1
/gate/output/root/setRootSinglesFlag 0
/gate/output/root/setRootNtupleFlag 0
/gate/output/verbose 2


#####################################################################
#   S T A R T                                                   
#####################################################################
# Start the simulation and select random seed
/gate/random/setEngineName MersenneTwister
/gate/random/setEngineSeed manual
/gate/random/setEngineSeed 9000
/gate/application/start
