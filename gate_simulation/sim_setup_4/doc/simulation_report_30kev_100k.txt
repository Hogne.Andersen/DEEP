Simulation Time: 366 s

========================================================================
==                          Simulation Report                         ==
========================================================================
Absorbed energy in front pixels:                2678719.500 keV 
Absorbed energy in back pixels:                 3085.753 keV 
Total Absorbed energy in pixels (f+b):          2683730.750 keV 

Total unresolved energy in pixels:              1318.060 keV 
Total energy absorbed in Si:                    2682642.000 keV 
========================================================================

