#####################################################################
##  File name: DEEP_GATE.mac                                         
##  Author: Hogne Andersen                                      
##  Description: Simulation of the pixels for the DEEP instrument 
##               with monoenergetic electron beam, detector house,
##               and a point source.     
##  GATE version: 8.1                                             
#####################################################################


#####################################################################
#   V I S U A L I S A T I O N                                   
#####################################################################
# Define visualisation elements
/vis/open                                   OGLSQt
/vis/viewer/zoom                            1
/vis/viewer/set/viewpointThetaPhi           120 120
/vis/viewer/set/style                       surface
/tracking/storeTrajectory                   1
/vis/scene/endOfEventAction accumulate      25000 
/vis/drawVolume
/vis/viewer/update
/vis/viewer/refresh 


#####################################################################
#   M A T E R I A L S                                           
#####################################################################
# Define where the material database is
/gate/geometry/setMaterialDatabase data/GateMaterials.db


##################################################################### 
#   W O R L D                                                   
#####################################################################
# Define the world dimensions
/gate/world/geometry/setXLength     90. mm
/gate/world/geometry/setYLength     90. mm
/gate/world/geometry/setZLength     90. mm
/gate/world/setMaterial             Vacuum
/gate/world/vis/setColor            gray
/gate/world/vis/forceWireframe      1
/gate/world/vis/setVisible          1


#####################################################################
#   D E T E C T O R    H O U S E                              
#####################################################################
# Create the electron detector house
/gate/world/daughters/name                          AluSide1
/gate/world/daughters/insert                        box
# Define the dimensions, position and material
/gate/AluSide1/geometry/setXLength                  40. mm
/gate/AluSide1/geometry/setYLength                  40. mm
/gate/AluSide1/geometry/setZLength                  2.5 mm
/gate/AluSide1/placement/setTranslation             0 0 -20.9 mm
/gate/AluSide1/setMaterial                          Aluminium
/gate/AluSide1/vis/setColor                         blue
/gate/AluSide1/vis/forceWireframe                   1
/gate/AluSide1/vis/setVisible                       1

/gate/world/daughters/name                          AluSide2
/gate/world/daughters/insert                        box
# Define the dimensions, position and material
/gate/AluSide2/geometry/setXLength                  40. mm
/gate/AluSide2/geometry/setYLength                  40. mm
/gate/AluSide2/geometry/setZLength                  2.5 mm
/gate/AluSide2/placement/setTranslation             0 0 8.6 mm
/gate/AluSide2/setMaterial                          Aluminium
/gate/AluSide2/vis/setColor                         blue
/gate/AluSide2/vis/forceWireframe                   1
/gate/AluSide2/vis/setVisible                       1

/gate/world/daughters/name                          AluSide3
/gate/world/daughters/insert                        box
# Define the dimensions, position and material
/gate/AluSide3/geometry/setXLength                  32. mm
/gate/AluSide3/geometry/setYLength                  40. mm
/gate/AluSide3/geometry/setZLength                  2.5 mm
/gate/AluSide3/placement/setTranslation             -18.75 0 -6.25 mm
/gate/AluSide3/placement/setRotationAxis            0 1 0 
/gate/AluSide3/placement/setRotationAngle           90 deg  
/gate/AluSide3/setMaterial                          Aluminium
/gate/AluSide3/vis/setColor                         blue
/gate/AluSide3/vis/forceWireframe                   1
/gate/AluSide3/vis/setVisible                       1

/gate/world/daughters/name                          AluSide4
/gate/world/daughters/insert                        box
# Define the dimensions, position and material
/gate/AluSide4/geometry/setXLength                  32. mm
/gate/AluSide4/geometry/setYLength                  40. mm
/gate/AluSide4/geometry/setZLength                  2.5 mm
/gate/AluSide4/placement/setTranslation             18.75 0 -6.25 mm
/gate/AluSide4/placement/setRotationAxis            0 1 0 
/gate/AluSide4/placement/setRotationAngle           90 deg  
/gate/AluSide4/setMaterial                          Aluminium
/gate/AluSide4/vis/setColor                         blue
/gate/AluSide4/vis/forceWireframe                   1
/gate/AluSide4/vis/setVisible                       1

/gate/world/daughters/name                          AluSide5
/gate/world/daughters/insert                        box
# Define the dimensions, position and material
/gate/AluSide5/geometry/setXLength                  40. mm
/gate/AluSide5/geometry/setYLength                  32. mm
/gate/AluSide5/geometry/setZLength                  2.5 mm
/gate/AluSide5/placement/setTranslation             0 18.75 -6.25 mm
/gate/AluSide5/placement/setRotationAxis            1 0 0 
/gate/AluSide5/placement/setRotationAngle           90 deg  
/gate/AluSide5/setMaterial                          Aluminium
/gate/AluSide5/vis/setColor                         blue
/gate/AluSide5/vis/forceWireframe                   1
/gate/AluSide5/vis/setVisible                       1

/gate/world/daughters/name                          AluSide6
/gate/world/daughters/insert                        box
# Define the dimensions, position and material
/gate/AluSide6/geometry/setXLength                  40. mm
/gate/AluSide6/geometry/setYLength                  32. mm
/gate/AluSide6/geometry/setZLength                  2.5 mm
/gate/AluSide6/placement/setTranslation             0 -18.75 -6.25 mm
/gate/AluSide6/placement/setRotationAxis            1 0 0 
/gate/AluSide6/placement/setRotationAngle           90 deg  
/gate/AluSide6/setMaterial                          Aluminium
/gate/AluSide6/vis/setColor                         blue
/gate/AluSide6/vis/forceWireframe                   1
/gate/AluSide6/vis/setVisible                       1

/gate/world/daughters/name                          WolframSide1
/gate/world/daughters/insert                        box
# Define the dimensions, position and material
/gate/WolframSide1/geometry/setXLength              35. mm
/gate/WolframSide1/geometry/setYLength              35. mm
/gate/WolframSide1/geometry/setZLength              2. mm
/gate/WolframSide1/placement/setTranslation         0 0 -18.4 mm
/gate/WolframSide1/setMaterial                      Tungsten
/gate/WolframSide1/vis/setColor                     green
/gate/WolframSide1/vis/forceWireframe               1
/gate/WolframSide1/vis/setVisible                   1

/gate/world/daughters/name                          WolframSide2
/gate/world/daughters/insert                        box
# Define the dimensions, position and material
/gate/WolframSide2/geometry/setXLength              35. mm
/gate/WolframSide2/geometry/setYLength              35. mm
/gate/WolframSide2/geometry/setZLength              2. mm
/gate/WolframSide2/placement/setTranslation         0 0 6.6 mm
/gate/WolframSide2/setMaterial                      Tungsten
/gate/WolframSide2/vis/setColor                     green
/gate/WolframSide2/vis/forceWireframe               1
/gate/WolframSide2/vis/setVisible                   1

/gate/world/daughters/name                          WolframSide3
/gate/world/daughters/insert                        box
# Define the dimensions, position and material
/gate/WolframSide3/geometry/setXLength              27. mm
/gate/WolframSide3/geometry/setYLength              35. mm
/gate/WolframSide3/geometry/setZLength              2. mm
/gate/WolframSide3/placement/setTranslation         -16.75 0 -6.1 mm
/gate/WolframSide3/placement/setRotationAxis        0 1 0 
/gate/WolframSide3/placement/setRotationAngle       90 deg 
/gate/WolframSide3/setMaterial                      Tungsten
/gate/WolframSide3/vis/setColor                     green
/gate/WolframSide3/vis/forceWireframe               1
/gate/WolframSide3/vis/setVisible                   1

/gate/world/daughters/name                          WolframSide4
/gate/world/daughters/insert                        box
# Define the dimensions, position and material
/gate/WolframSide4/geometry/setXLength              27. mm
/gate/WolframSide4/geometry/setYLength              35. mm
/gate/WolframSide4/geometry/setZLength              2. mm
/gate/WolframSide4/placement/setTranslation         16.75 0 -6.1 mm
/gate/WolframSide4/placement/setRotationAxis        0 1 0 
/gate/WolframSide4/placement/setRotationAngle       90 deg 
/gate/WolframSide4/setMaterial                      Tungsten
/gate/WolframSide4/vis/setColor                     green
/gate/WolframSide4/vis/forceWireframe               1
/gate/WolframSide4/vis/setVisible                   1

/gate/world/daughters/name                          WolframSide5
/gate/world/daughters/insert                        box
# Define the dimensions, position and material
/gate/WolframSide5/geometry/setXLength              35. mm
/gate/WolframSide5/geometry/setYLength              27. mm
/gate/WolframSide5/geometry/setZLength              2.5 mm
/gate/WolframSide5/placement/setTranslation         0 -16.75 -6.25 mm
/gate/WolframSide5/placement/setRotationAxis        1 0 0 
/gate/WolframSide5/placement/setRotationAngle       90 deg  
/gate/WolframSide5/setMaterial                      Tungsten
/gate/WolframSide5/vis/setColor                     green
/gate/WolframSide5/vis/forceWireframe               1
/gate/WolframSide5/vis/setVisible                   1

/gate/world/daughters/name                          WolframSide6
/gate/world/daughters/insert                        box
# Define the dimensions, position and material
/gate/WolframSide6/geometry/setXLength              35. mm
/gate/WolframSide6/geometry/setYLength              27. mm
/gate/WolframSide6/geometry/setZLength              2.5 mm
/gate/WolframSide6/placement/setTranslation         0 16.75 -6.25 mm
/gate/WolframSide6/placement/setRotationAxis        1 0 0 
/gate/WolframSide6/placement/setRotationAngle       90 deg  
/gate/WolframSide6/setMaterial                      Tungsten
/gate/WolframSide6/vis/setColor                     green
/gate/WolframSide6/vis/forceWireframe               1
/gate/WolframSide6/vis/setVisible                   1


#####################################################################
#   E L E C T R O N    D E T E C T O R                             
#####################################################################
# Create the electron detector sensor
/gate/world/daughters/name                          scanner
/gate/world/daughters/insert                        box
# Define the dimensions, position and material
/gate/scanner/geometry/setXLength                   15. mm
/gate/scanner/geometry/setYLength                   15. mm
/gate/scanner/geometry/setZLength                   2.4 mm
/gate/scanner/placement/setTranslation              0 0 0 mm
/gate/scanner/vis/setColor                          red
/gate/scanner/vis/forceWireframe                    1
/gate/scanner/vis/setVisible                        0

# Define the dimensions, position and material
/gate/scanner/daughters/name                        SiFront
/gate/scanner/daughters/insert                      box
/gate/SiFront/geometry/setXLength                   15. mm
/gate/SiFront/geometry/setYLength                   15. mm
/gate/SiFront/geometry/setZLength                   1. mm
/gate/SiFront/placement/setTranslation              0 0 -0.7 mm
/gate/SiFront/setMaterial                           Silicon
/gate/SiFront/vis/setColor                          yellow
/gate/SiFront/vis/forceWireframe                    1
/gate/SiFront/vis/setVisible                        1

# Define the dimensions, position and material
/gate/scanner/daughters/name                        SiBack
/gate/scanner/daughters/insert                      box
/gate/SiBack/geometry/setXLength                    15. mm
/gate/SiBack/geometry/setYLength                    15. mm
/gate/SiBack/geometry/setZLength                    1. mm
/gate/SiBack/placement/setTranslation               0 0 0.7 mm
/gate/SiBack/setMaterial                            Silicon
/gate/SiBack/vis/setColor                           yellow
/gate/SiBack/vis/forceWireframe                     1
/gate/SiBack/vis/setVisible                         1

# Attach all layers to scanner
/gate/SiFront/attachCrystalSD
/gate/SiBack/attachCrystalSD


#####################################################################
#   P H A N T O M    S O U R C E                                
#####################################################################
# Create a phantom for the source for easier placement 
/gate/world/daughters/name                          phantomSource
/gate/world/daughters/insert                        box
# Define the dimensions, position and material  
/gate/phantomSource/geometry/setXLength             5 mm
/gate/phantomSource/geometry/setYLength             5 mm
/gate/phantomSource/geometry/setZLength             1 mm
/gate/phantomSource/placement/setTranslation        0. 0. -16.4 mm 
/gate/phantomSource/placement/setRotationAxis       1 0 0 
/gate/phantomSource/placement/setRotationAngle      180 deg  
/gate/phantomSource/setMaterial                     Vacuum
/gate/phantomSource/vis/setColor                    green
/gate/phantomSource/vis/forceWireframe              1
/gate/phantomSource/vis/setVisible                  1


#####################################################################
#   P H Y S I C S                                               
#####################################################################
# Define physics to be emstandard_opt4
/gate/physics/addPhysicsList emstandard_opt4


#####################################################################
#   I N I T I A L I Z E                                         
#####################################################################
/gate/run/initialize


#####################################################################
#   S O U R C E                                                     
#####################################################################
# Define a uniform mono energy electron beam
/gate/source/addSource                              uniformBeam gps
/gate/source/uniformBeam/gps/particle               e-
/gate/source/uniformBeam/gps/energytype             Mono
/gate/source/uniformBeam/gps/monoenergy             {energy} keV
/gate/source/uniformBeam/gps/type                   Point
/gate/source/uniformBeam/gps/pos/centre             0 0 0 mm
/gate/source/uniformBeam/gps/direction              0 0 1
/gate/source/uniformBeam/gps/centre                 0. 0. 0. mm
/gate/source/uniformBeam/gps/angtype                iso
/gate/source/uniformBeam/gps/mintheta               0 deg
/gate/source/uniformBeam/gps/maxtheta               13.2 deg
/gate/source/uniformBeam/gps/minphi                 0 deg
/gate/source/uniformBeam/gps/maxphi                 360 deg
/gate/source/uniformBeam/attachTo                   phantomSource
/gate/application/setTotalNumberOfPrimaries         {particles}
/gate/source/uniformBeam/visualize                  25000 green 1 


#####################################################################
#   O U T P U T                                                 
#####################################################################
# Define output as ROOT file
/gate/output/root/enable # enable root output file
/gate/output/root/setFileName output/DEEP_OUTPUT

/gate/output/root/setRootHitFlag 1
/gate/output/root/setRootSinglesFlag 0
/gate/output/root/setRootNtupleFlag 0
/gate/output/verbose 2


#####################################################################
#   S T A R T                                                   
#####################################################################
# Start the simulation and select random seed
/gate/random/setEngineName MersenneTwister
/gate/random/setEngineSeed auto
/gate/application/start
