# 2 mm Thick Silicon Layer
Simulation of the pixels for the DEEP instrument with monoenergetic electron beam, and a 2 mm thick Silicon layer.

### Repository structure
    2mm_thick_silicon_layer             # execution from this folder
    ├── output                          # results
    ├── mac                             # macrofile for GATE
    ├── data                            # material database for GATE
    ├── src                             # macro and readout file for ROOT
    └── README.md

#### To start a simulation:
   ```
   Gate mac/DEEP_GATE.mac -a '[energy, X] [particles, Y]'
   ```
Where `X` is energy in keV, and `Y` is number of particles.
   
#### To start the readout process: 
   ```
   root -q -b src/DEEP_ROOT_MACRO.cxx 
   ```
   
#### To visualize the simulation geometry:
   ```
   Gate --qt
   ```
Followed by opening the mac/DEEP_GATE.mac file.

