Simulation Time: 407 s

========================================================================
==                          Simulation Report                         ==
========================================================================
Absorbed energy in front pixels:                2689444.500 keV 
Absorbed energy in back pixels:                 3289.533 keV 
Total Absorbed energy in pixels (f+b):          2694624.750 keV 

Total unresolved energy in pixels:              1183.911 keV 
Total energy absorbed in Si:                    2693453.500 keV 
========================================================================

