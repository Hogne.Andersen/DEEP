Simulation Time: 1789 s

========================================================================
==                          Simulation Report                         ==
========================================================================
Absorbed energy in front pixels:                70781440.000 keV 
Absorbed energy in back pixels:                 19730464.000 keV 
Total Absorbed energy in pixels (f+b):          90668456.000 keV 

Total unresolved energy in pixels:              1552.741 keV 
Total energy absorbed in Si:                    90192112.000 keV 
========================================================================

