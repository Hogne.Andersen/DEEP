# Simulation Setup 2
Simulation of the pixels for the DEEP instrument with monoenergetic electron beam and detector house.

### Repository structure
    sim_setup_2                         # execution from this folder
    ├── output                          # .ROOT and .csv results
    │   ├── 30kev_100k
    │   ├── 60kev_100k
    │   ├── 120kev_100k
    │   ├── 240kev_100k
    │   ├── 480kev_100k
    │   ├── 960kev_100k
    │   └── 1920kev_100k
    ├── mac                             # macrofile for GATE
    ├── data                            # material database for GATE
    ├── src                             # macro and readout file for ROOT
    ├── doc                             # simulation and readout reports
    └── README.md

#### To start a simulation:
   ```
   Gate mac/DEEP_GATE.mac -a '[energy, X] [particles, Y]'
   ```
Where `X` is energy in keV, and `Y` is number of particles.
   
#### To start the readout process: 
   ```
   root -q -b src/DEEP_ROOT_MACRO.cxx 
   ```
   
#### To visualize the simulation geometry:
   ```
   Gate --qt
   ```
Followed by opening the mac/DEEP_GATE.mac file.
