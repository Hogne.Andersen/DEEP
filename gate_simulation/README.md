# GATE Simulation Repository
> For the Distribution of Energetic Electron and Proton Instrument

### Simulation Setup 1
Simulation of the pixels for the DEEP instrument with monoenergetic electron beam. <br />
More detailed information: [sim_setup_1](https://git.app.uib.no/Hogne.Andersen/DEEP/tree/master/gate_simulation/sim_setup_1/)

### Simulation Setup 2
Simulation of the pixels for the DEEP instrument with monoenergetic electron beam and detector house. <br />
More detailed information: [sim_setup_2](https://git.app.uib.no/Hogne.Andersen/DEEP/tree/master/gate_simulation/sim_setup_2/)

### Simulation Setup 3
Simulation of the pixels for the DEEP instrument with monoenergetic electron beam, and a point source. <br />
More detailed information: [sim_setup_3](https://git.app.uib.no/Hogne.Andersen/DEEP/tree/master/gate_simulation/sim_setup_3/)

### Simulation Setup 4
Simulation of the pixels for the DEEP instrument with monoenergetic electron beam, detector house, and a point source. <br />
More detailed information: [sim_setup_4](https://git.app.uib.no/Hogne.Andersen/DEEP/tree/master/gate_simulation/sim_setup_4/)

### Simulation Setup Wide
Simulation of the pixels for the DEEP instrument with monoenergetic electron beam, detector house, and a point source. <br />
More detailed information: [sim_setup_wide](https://git.app.uib.no/Hogne.Andersen/DEEP/tree/master/gate_simulation/sim_setup_wide/)

### Simulation Setup Baffle
Simulation of the pixels for the DEEP instrument with monoenergetic electron beam, detector house, point source, and baffles. <br />
More detailed information: [sim_setup_baffle](https://git.app.uib.no/Hogne.Andersen/DEEP/tree/master/gate_simulation/sim_setup_baffle/)

### Simulation Setup Mask
Simulation of the pixels for the DEEP instrument with monoenergetic electron beam, detector house, and a mask attached to the front layer. <br />
More detailed information: [sim_setup_mask](https://git.app.uib.no/Hogne.Andersen/DEEP/tree/master/gate_simulation/sim_setup_mask/)

### Physics List Validation
Physics List Validation setup. <br />
More detailed information: [physics_list_validation](https://git.app.uib.no/Hogne.Andersen/DEEP/tree/master/gate_simulation/physics_list_validation/)

### 2 mm Thick Silicon Layer
Simulation of the pixels for the DEEP instrument with monoenergetic electron beam, and a 2 mm thick Silicon layer. <br />
More detailed information: [2mm_thick_silicon_layer](https://git.app.uib.no/Hogne.Andersen/DEEP/tree/master/gate_simulation/2mm_thick_silicon_layer/)

### Proton Range
Proton/Electron Range Simulation. <br />
More detailed information: [proton_range](https://git.app.uib.no/Hogne.Andersen/DEEP/tree/master/gate_simulation/proton_range/)