/*
==========================================================================
    * File name: DEEP_READOUT.C
    * Author: Hogne Andersen
    * Description: Readout ROOT script for the simulation of the pixels
    *              for the DEEP instrument.
    * ROOT version: 6.13/02
==========================================================================
*/

#include <TTree.h>
#include <TFile.h>
#include <TCanvas.h>
#include <TLine.h>
#include <stdio.h>
#include <stdlib.h>

using namespace std;

/* Initialize struct */
typedef struct {
    int eID=0;
    float firstX=0, firstY=0, firstZ=0, lastX=0, lastY=0, lastZ=0, pX[19]={0}, sumE=0;
} output;

/* Initialize global variables */
int eventID=0, parentID=0;
float x=0, y=0, z=0, edep=0;
float edep_kev=0, energyFront=0, energyBack=0, energyTotal=0, totAbsSi=0, unresolvedEnergy=0;
output tmp;
FILE* fp;

/* Check x, y, and z coordinates, and put deposited energy into correct pixel array */
void calculatePixelEnergy() {
    /* Front pixels */
    if ((z>-1.2) && (z<-0.2)) { 
        /* Pixel 1-3 */
        if (y>2.5 && y<7.5) { 
            if (x>-7.5 && x<-2.5) {
                tmp.pX[1] += edep_kev;
            } else if (x>-2.5 && x<2.5) {
                tmp.pX[2] += edep_kev;
            } else if (x>2.5 && x<7.5) {
                tmp.pX[3] += edep_kev;
            } else {
            }  
        /* Pixel 4-6 */              
        } else if (y>-2.5 && y<2.5) { 
            if (x>-7.5 && x<-2.5) {
                tmp.pX[4] += edep_kev;
            } else if (x>=-2.5 && x<2.5) {
                tmp.pX[5] += edep_kev;
            } else if (x>2.5 && x<7.5) {
                tmp.pX[6] += edep_kev;
            } else {
            }     
        /* Pixel 7-9 */    
        } else if (y>-7.5 && y<-2.5) { 
            if (x>-7.5 && x<-2.5) {
                tmp.pX[7] += edep_kev;
            } else if (x>-2.5 && x<2.5) {
                tmp.pX[8] += edep_kev;
            } else if (x>2.5 && x<7.5) { 
                tmp.pX[9] += edep_kev;
            } else {
            }   
        } else {
        }  
    /* Back pixels */    
    } else if ((z>0.2) && (z<1.2)) {
        /* Pixel 10-12 */
        if (y>2.5 && y<7.5) {
            if (x>-7.5 && x<-2.5) {
                tmp.pX[10] += edep_kev;
            }
            else if (x>-2.5 && x<2.5) {
                tmp.pX[11] += edep_kev;
            }      
            else if (x>2.5 && x<7.5) {
                tmp.pX[12] += edep_kev;
            } else {
            }               
        /* Pixel 13-15 */
        } else if (y>-2.5 && y<2.5) { 
            if (x>-7.5 && x<-2.5) {
                tmp.pX[13] += edep_kev;
            } else if (x>=-2.5 && x<2.5) {
                tmp.pX[14] += edep_kev;
            } else if (x>2.5 && x<7.5) {
                tmp.pX[15] += edep_kev;
            } else {
            }  
        /* Pixel 16-18 */
        } else if (y>-7.5 && y<-2.5) { 
            if (x>-7.5 && x<-2.5) {
                tmp.pX[16] += edep_kev;
            } else if (x>-2.5 && x<2.5) {
                tmp.pX[17] += edep_kev;
            } else if (x>2.5 && x<7.5) { 
                tmp.pX[18] += edep_kev;
            } else {
            }
        } else {
        }    
    } else {
       unresolvedEnergy += edep_kev;
    }       
}

/* For each eventID sum up all deposited energy in the pixels */
void calculateTotalEnergy() {
    /* Absorbed energy in front pixels */
    for (int i=1; i<9; ++i) {
        energyFront += tmp.pX[i];
    }
    /* Absorbed energy in back pixels */  
    for (int i=10; i<19; ++i) {
        energyBack += tmp.pX[i];
    }
    /* Total absorbed energy (f+b) */    
    for (int i=1; i<19; ++i) {
        energyTotal += tmp.pX[i];
    }
    /* Total energy absorbed each event */
    for (int i=0; i<19; ++i) {
        tmp.sumE += tmp.pX[i];
    }
}

/* Clear the temporary array before next event */
void clearMemory() {
    for (int i=0; i<19; ++i) {
        tmp.pX[i]=0;
    }    
    tmp.sumE=0; 
}

/* Print simulation report to terminal */ 
void printToTerminal() {  
    printf("\n");
    printf("========================================================================\n");
    printf("==                          Simulation Report                         ==\n");
    printf("========================================================================\n");    
    printf("Absorbed energy in front pixels:                %.3f keV \n", energyFront);
    printf("Absorbed energy in back pixels:                 %.3f keV \n", energyBack);
    printf("Total Absorbed energy in pixels (f+b):          %.3f keV \n", energyTotal);
    printf("\n");
    printf("Total unresolved energy in pixels:              %.3f keV \n", unresolvedEnergy);
    printf("Total energy absorbed in Si:                    %.3f keV \n", totAbsSi);
    printf("========================================================================\n");      
    printf("\n");
}

/* Open the output file and set the header */
void openOutputFile() {
    fp = fopen ("output/DEEP_OUTPUT.csv","w");
    /* Set header in output file*/
    fprintf(fp,"eID,firstX,firstY,firstZ,lastX,lastY,lastZ,");
    for(int i=1; i < 19; ++i) {
        fprintf(fp,"P%d,", i);
    }
    fprintf(fp,"sumE\n");
    fclose(fp);
    fp = fopen ("output/DEEP_OUTPUT.csv","a");
}

/* For each event write the calculated data into the output file */
void writeToOutputFile() {
    fprintf(fp,"%d,%-.3f,%-.3f,%-.3f,%-.3f,%-.3f,%-.3f,", tmp.eID, tmp.firstX, tmp.firstY, tmp.firstZ, tmp.lastX, tmp.lastY, tmp.lastZ);
    for(int i=1; i < 19; ++i) {
        fprintf(fp,"%-.3f,", tmp.pX[i]);
    }
    fprintf(fp,"%-.3f\n", tmp.sumE);
    fflush(fp);    
}

/* This is the main program */
int ReadOut() {
    openOutputFile(); // Initialize output file

    /* Initialize ROOT */
    TFile* f1 = new TFile("output/DEEP_OUTPUT.root");
    TTree* htree = (TTree*)f1->Get("Hits");

    htree->SetBranchAddress("posX", &x);
    htree->SetBranchAddress("posY", &y);
    htree->SetBranchAddress("posZ", &z);
    htree->SetBranchAddress("edep", &edep);
    htree->SetBranchAddress("eventID", &eventID);
    htree->SetBranchAddress("parentID", &parentID); 

    /* Initialize local variables */
    int eventCount = 0, firstCornerStone = 1;

    for (int i=0; i<htree->GetEntries(); ++i) { // Loop through all data from the ROOT file
        htree->GetEntry(i); // Function that gets eventID, x, y, z, edep etc.
        //if (parentID != 0) continue; // Used to only show results from primary particles
        edep_kev = edep*1000; // Convert from MeV to keV
        totAbsSi += edep_kev;

        /* Print progress to terminal */
        printf("\33[2K\rCalculated eventID: %d", eventID);
        fflush(stdout);       
        
        /* First cornerstone - on the first interaction on the first eventID */
        if ((eventCount == eventID) && firstCornerStone ) {
            /* Get the first x,y and z coordinate */
            tmp.firstX = x;
            tmp.firstY = y;
            tmp.firstZ = z;
            tmp.eID  = eventID;
            eventCount++; // Increase event number to only do this case once (on first) per event
            firstCornerStone = 0;
            calculatePixelEnergy();

        /* Last entry in the event */
        } else if (eventCount == eventID) {
            calculateTotalEnergy();
            writeToOutputFile(); // Works if function is NOT dependent on htree
            clearMemory(); // Clean up before next event
            
            /* Get the first x,y and z coordinate */            
            tmp.firstX = x;
            tmp.firstY = y;
            tmp.firstZ = z;
            tmp.eID  = eventID;

            eventCount++;
            calculatePixelEnergy();

        /* This is the "normal" case */
        } else { 
            calculatePixelEnergy();
            /* Get the last x,y and z coordinate */
            tmp.lastX = x;
            tmp.lastY = y;
            tmp.lastZ = z;
        } 
    }

    /* Last cornerstone - at the end of last eventID */
    calculateTotalEnergy();
    tmp.lastX = x;
    tmp.lastY = y;
    tmp.lastZ = z;

    /* Write to output file */
    writeToOutputFile();

    /* Print simulation report to terminal */
    printToTerminal();
    
    /* Close output file */
    fclose (fp);
    
    /* Clean up ROOT */
    delete f1;
    return 0;
}