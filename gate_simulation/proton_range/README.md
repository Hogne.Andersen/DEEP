# Proton Range
Proton/Electron Range Simulation.

### Repository structure
    proton_range                        # execution from this folder
    ├── output                          # results
    ├── mac                             # macrofile for GATE
    ├── data                            # material database for GATE
    ├── src                             # macro and readout file for ROOT
    └── README.md

#### To start a simulation:
   ```
   Gate mac/DEEP_GATE.mac -a '[energy, X] [particles, Y]'
   ```
Where `X` is energy in keV, and `Y` is number of particles.
   
#### To start the readout process: 
   ```
   root -q -b src/DEEP_ROOT_MACRO.cxx 
   ```
   
#### To visualize the simulation geometry:
   ```
   Gate --qt
   ```
Followed by opening the mac/DEEP_GATE.mac file.


